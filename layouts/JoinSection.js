import { Component } from 'react'
import Button from '../components/Button'
import JoinSvg from '../static/images/draw/join.svg'
import ProfitSvg from '../static/images/draw/profit.svg'
import GetTockensSvg from '../static/images/draw/get-tokens.svg'

export default class JoinSection extends Component{
    render(){
        return(
            <section id="faq" className="join-section">
                <div className="container">
                    <div className="row">
                        <div className="col-12 reveal">
                            <h2 className="title title--default join__title">How to join?</h2>
                        </div>
                    </div>
                    <div className="row justify-content-center">
                        <div className="col-xl-3 reveal">
                            <div className="join__description">
                                <p>Choosing representative digital assets to offer cross-section can be a challenge.
                                    The Theindex.fund represents a constantly adjusted cross-section of the new economy
                                    with the most potential.</p>
                            </div>
                        </div>
                        <div className="col-md-4 col-lg-3 reveal">
                            <div className="join__item">
                                <div className="join__figure js-draw">
                                    <JoinSvg className="join__image join__image--1" icon="join" />
                                </div>
                                <div className="join__subtitle">Join</div>
                                <img src="/static/images/arrow-svg.svg" alt="arrow" className="join__arrow"/>
                            </div>
                        </div>
                        <div className="col-md-4 col-lg-3 reveal">
                            <div className="join__item">
                                <div className="join__figure js-draw">
                                    <GetTockensSvg className="join__image join__image--2" icon="get-tockens" />
                                </div>
                                <div className="join__subtitle">Get Tokens</div>
                                <img src="/static/images/arrow-svg.svg"
                                     alt="arrow" className="join__arrow"/>
                            </div>
                        </div>
                        <div className="col-md-4 col-lg-3 reveal">
                            <div className="join__item join__item--last">
                                <div className="join__figure js-draw">
                                    <ProfitSvg className="join__image join__image--3" icon="profit" />
                                </div>
                                <div className="join__subtitle">Take Profit</div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 text--center reveal">
                            <Button className='btn--regular'>
                                Invest Now
                            </Button>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}