import { Component } from 'react'
import Icon from '../components/SvgIcon'
import ScrollToElement from '../helper/scrollto'

export default class Footer extends Component{
    render(){
        return(

            <footer className="footer">
                <div className="container">
                    <div className="row">
                        <div className="col-12 ">
                            <div className="footer__social">
                                <a className="footer__social-link" href="#">
                                    <Icon className="footer__social-svg" icon="envelope" />
                                </a>
                                <a className="footer__social-link" href="#">
                                    <Icon className="footer__social-svg" icon="facebook-logo" />
                                </a>
                                <a className="footer__social-link" href="#">
                                    <Icon className="footer__social-svg" icon="telegram" />
                                </a>
                            </div>
                            <p>
                                THE INFORMATION PROVIDED ON THIS WEBSITE IS NOT INTENDED FOR DISTRIBUTION TO, OR USE BY,
                                ANY PERSON OR ENTITY IN THE UNITED STATES OR THE REPUBLIC OF SINGAPORE, OR IN ANY
                                JURISDICTION OR COUNTRY WHERE SUCH DISTRIBUTION OR USE WOULD BE CONTRARY TO ANY LAW
                                OR REGULATION, OR WHICH WOULD SUBJECT THE TOKEN FUND AND/OR ENTITY CREATING THE TKN
                                TOKENS (INCLUDING THEIR AFFILIATES) OR ANY OF THEIR PRODUCTS OR SERVICES TO ANY
                                REGISTRATION, LICENSING OR OTHER AUTHORIZATION REQUIREMENT WITHIN SUCH JURISDICTION
                                OR COUNTRY.</p>
                            <p>
                                YOU ARE ONLY ALLOWED TO PURCHASE TKN TOKENS IF AND BY BUYING TKN TOKENS YOU COVENANT,
                                REPRESENT, AND WARRANT THAT YOU ARE NEITHER A U. S. CITIZEN OR PERMANENT RESIDENT OF
                                THE UNITED STATES, NOR DO YOU HAVE A PRIMARY RESIDENCE OR DOMICILE IN THE UNITED STATES,
                                INCLUDING PUERTO RICO, THE U. S. VIRGIN ISLANDS, AND ANY OTHER POSSESSIONS OF THE UNITED
                                STATES. IN ORDER TO BUY TKN TOKENS AND BY BUYING TKN TOKENS YOU COVENANT, REPRESENT, AND
                                WARRANT THAT NONE OF THE OWNERS OF THE COMPANY, OF WHICH YOU ARE AN AUTHORIZED OFFICER,
                                ARE U. S. CITIZEN OR PERMANENT RESIDENT OF THE UNITED STATES, NOR DO YOU HAVE A PRIMARY
                                RESIDENCE OR DOMICILE IN THE UNITED STATES, INCLUDING PUERTO RICO, THE U. S. VIRGIN ISLANDS,
                                AND ANY OTHER POSSESSIONS OF THE UNITED STATES. SHOULD THIS CHANGE AT ANY TIME, YOU SHALL
                                IMMEDIATELY NOTIFY THE CREATOR OF TKN TOKENS.
                            </p>
                            <p>
                                CREATOR OF TKN TOKENS SHALL RESERVE THE RIGHT TO REFUSE SELLING TKN TOKENS TO ANYONE WHO
                                DOES NOT MEET CRITERIA NECESSARY FOR THEIR BUYING, AS SET OUT HEREUNDER AND BY THE
                                APPLICABLE LAW. IN PARTICULAR, THE CREATOR OF TKN TOKENS MAY REFUSE SELLING TKN TOKENS
                                TO U. S. CITIZENS, PERMANENT RESIDENTS OF THE UNITED STATES AND THOSE USERS WHO DO NOT
                                MEET ELIGIBILITY CRITERIA ESTABLISHED BY THE CREATOR OF TKN TOKENS FROM TO TIME IN ITS
                                SOLE DISCRETION.
                            </p>
                            <p>
                                The data content of this website is intended for general information purposes only and does
                                not constitute solicitation of or an offer to purchase any securities. This website should
                                be used for general research purposes only. It does not, nor does it purport to, constitute
                                any form of professional investment advice, recommendation or independent analysis.
                                Consequently, the information contained on this website has not been prepared
                                in accordance with the relevant rules and regulations governing such publications
                                in various jurisdictions.
                            </p>
                            <p className="footer__logo-container">
                                <a href="#" onClick={(e) => {
                                    e.preventDefault();
                                    new ScrollToElement().scrollToY(0);
                                }}>
                                    <img className="footer__logo" src="/static/images/logo.svg" alt="logo"/>
                                </a>
                            </p>
                            <p className="footer__copyright">
                                © TheIndex.Fund. 2018
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
        )
    }
}