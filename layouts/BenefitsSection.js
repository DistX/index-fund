import { Component } from 'react'
import Icon from '../components/SvgIcon'
import HighPrfomanceSvg from '../static/images/draw/perfomance.svg'
import SecuritySvg from '../static/images/draw/security.svg'
import DiversificationSvg from '../static/images/draw/diversification.svg'
import LiquiditySvg from '../static/images/draw/liquidity.svg'



export default class BenefitsSection extends Component{

    render(){
        return(
            <section id="benefits" className="benefits-section">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <h2 className="title title--white benefits__title reveal">Benefits</h2>
                        </div>
                    </div>
                    <div className="row justify-content-center">
                        <div className="col-6 col-md-5 col-lg-3 reveal">
                            <div className="benefit js-draw">
                                <div className="benefit__figure">
                                    <HighPrfomanceSvg className="benefit__image benefit__image--1" />
                                </div>
                                <div className="benefit__title">
                                    High Performance
                                </div>
                                <p className="benefit__description">
                                    Cryptocurrency market is growing faster than the classic stock market
                                </p>
                            </div>
                        </div>
                        <div className="col-6 col-md-5 col-lg-3 reveal">
                            <div className="benefit js-draw">
                                <div className="benefit__figure">
                                    <SecuritySvg className="benefit__image benefit__image--2" icon="security" />
                                </div>
                                <div className="benefit__title">
                                    Security
                                </div>
                                <p className="benefit__description">
                                    100% of funds are stored in multisignature wallets
                                </p>
                            </div>
                        </div>
                        <div className="col-6 col-md-5 col-lg-3 reveal">
                            <div className="benefit js-draw">
                                <div className="benefit__figure">
                                    <DiversificationSvg className="benefit__image benefit__image--3" icon="diversification" />
                                </div>
                                <div className="benefit__title">
                                    Diversification
                                </div>
                                <p className="benefit__description">
                                    The Index Plus portfolio is comprised of a broad range of cryptoassets thereby minimizing risks
                                </p>
                            </div>
                        </div>
                        <div className="col-6 col-md-5 col-lg-3 reveal">
                            <div className="benefit js-draw">
                                <div className="benefit__figure">
                                    <LiquiditySvg className="benefit__image benefit__image--4" icon="liquidity" />
                                </div>
                                <div className="benefit__title">
                                    Liquidity
                                </div>
                                <p className="benefit__description">
                                    The Index Plus token is backed by the corresponding portfolio and can be purchased or redeemed within a short period of time
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}