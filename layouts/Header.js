import { Component } from 'react'
import LangSwitcher from '../components/LangSwitcher'
import LoginBtn from '../components/LoginBtn'
import ScrollToElement from '../helper/scrollto'
import MenuComp from '../components/Menu'


export default class Header extends Component{

    constructor(props) {
        super(props);
        this.state = {menuIsActive: false};
        this.scroller = new ScrollToElement();
    }
    scrollTo(e){
        e.preventDefault();
        this.setState({menuIsActive: false});
        let target = e.target.getAttribute('href');
        let el = document.querySelector(target);
        let header = document.querySelector('.header');
        header.classList.add('header--fixed');
        this.scroller.scrollToY(el.offsetTop - header.offsetHeight);
    }

    render(){
        return(
            <header className="header ">
                <div className="container header__container">
                    <div className="row align-items-center justify-content-between">
                        <div className="col col-lg-4">
                            <a href="#" onClick={(e)=>{
                                e.preventDefault();
                                this.scroller.scrollToY(0);
                            }}>
                                <img src="/static/images/logo.svg" className="header__logo" alt="logo"/>
                            </a>
                        </div>
                        <div className="col-lg-4 header__xl-nav">
                            <nav className="header__menu">
                                <ul>
                                    <li className="header__menu-item">
                                        <a href="#how-it-works" onClick={this.scrollTo.bind(this)}>How it works</a>
                                    </li>
                                    <li className="header__menu-item">
                                        <a href="#benefits" onClick={this.scrollTo.bind(this)}>Benefits</a>
                                    </li>
                                    <li className="header__menu-item">
                                        <a href="#faq" onClick={this.scrollTo.bind(this)}>FAQ</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div className="col col-xl-4 header__action-btns">
                            <LangSwitcher />
                            <LoginBtn />

                        </div>
                        <div className="header__menu-btn-container">
                            <div className="header__menu-btn" onClick={() => {
                                this.setState({menuIsActive: !this.state.menuIsActive})
                            }}>
                                <span className={this.state.menuIsActive ? 'header__menu-row header__menu-row--active' : 'header__menu-row'}></span>
                                <span className={this.state.menuIsActive ? 'header__menu-row header__menu-row--active' : 'header__menu-row'}></span>
                                <span className={this.state.menuIsActive ? 'header__menu-row header__menu-row--active' : 'header__menu-row'}></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={this.state.menuIsActive ? 'mob-menu active' : 'mob-menu'}>
                    <div>
                        <LoginBtn />
                        <nav className="header__menu">
                            <ul>
                                <li className="header__menu-item">
                                    <a href="#how-it-works" onClick={this.scrollTo.bind(this)}>How it works</a>
                                </li>
                                <li className="header__menu-item">
                                    <a href="#benefits" onClick={this.scrollTo.bind(this)}>Benefits</a>
                                </li>
                                <li className="header__menu-item">
                                    <a href="#faq" onClick={this.scrollTo.bind(this)}>FAQ</a>
                                </li>
                            </ul>
                        </nav>
                        <LangSwitcher />
                    </div>
                </div>
            </header>
        )
    }
}