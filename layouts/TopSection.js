import React, { Component } from 'react'

export default class TopSection extends Component{
    render(){
        return (
            <div className="top">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6 col-lg-4"></div>
                        <div className="col-md-6 col-lg-4"></div>
                        <div className="col-lg-4"></div>
                    </div>
                </div>
            </div>
        )
    }
}