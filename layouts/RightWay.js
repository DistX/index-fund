import { Component } from 'react'
import Button from '../components/Button'

export default class RightWay extends Component{
    render(){

        return(
            <section className="right-way">
                <div className="container">
                    <div className="row right-way__row">
                        <div className="col-4 col-md-5 col-xl-4 reveal" >
                            <div className="compass-container">
                                <img src="/static/images/compass-svg.svg" alt="compass"/>
                            </div>
                        </div>
                        <div className="col-8 col-md-7 col-xl-8 reveal">
                            <p className="right-way__title">
                                <span className="right-way__title-attension">THE RIGHT WAY</span> TO JOIN DIGITAL <span className="right-way__title-smaller">CURRENCY WORLD</span>
                            </p>
                            <div className="right-way__btn--769">
                                <Button href="#" className="btn--white">
                                    Invest Now
                                </Button>
                            </div>
                        </div>
                    </div>
                    <div className="row right-way__btn--576 reveal">
                        <div className="col-12  text--center">
                            <Button className="btn--white">
                                Invest Now
                            </Button>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}