import { Component } from 'react'
import Scheme from '../static/images/scheme-720-svg.svg'
import SchemeFull from '../static/images/scheme-full.svg'

export default class HowSection extends Component{
    componentDidMount(){

    }

    render(){
        return(
            <section id="how-it-works" className="how-it-works">
                <div className="container">
                    <div className="row">
                        <div className="col-12 reveal">
                            <h2 className="title title--default how-it-works__title">How It Works?</h2>
                        </div>
                    </div>
                    <div className="row how-it-works__description">
                        <div className="col-md-6 reveal" ref="reveal">
                            <p>
                                When the investor transfers to one of the personal investment addresses (ETH, BTC or LTC),
                                which are displayed in the personal account, the entrance fee (2%) is deducted from the
                                received amount. Then, the investor's crypto currency will be converted into
                                Tokens at the current exchange rate.
                            </p>
                        </div>
                        <div className="col-md-6 reveal" ref="reveal">
                            <p>
                                At the time of the conversion of the Crypto-currency into Tokens, the amount of the
                                Crypto-currency, expressed in USD at the average inter-exchange rate at the time of
                                conversion, is divided by the cost of one Token, so the number of new Tokens that are
                                assigned to the investor is determined.
                            </p>
                        </div>
                    </div>
                </div>
                <div className="how-it-works__scheme reveal">
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-md-8 col-lg-12">
                                <div className="scheme-middle">
                                    <Scheme />
                                </div>
                                <div className="scheme-full">
                                    <SchemeFull />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}