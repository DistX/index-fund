import Document, { Head, Main, NextScript } from 'next/document'
import { ServerStyleSheet } from 'styled-components'

export default class MyDocument extends Document {
    static getInitialProps ({ renderPage }) {
        const sheet = new ServerStyleSheet()
        const page = renderPage(App => props => sheet.collectStyles(<App {...props} />))
        const styleTags = sheet.getStyleElement()
        return { ...page, styleTags }
    }

    render () {
        return (
            <html>
            <Head>
                <title>The Index Fund</title>
                <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
                <script type="text/javascript" src="/static/charting_library/charting_library.min.js"></script>
                <script type="text/javascript" src="/static/charting_library/datafeed/udf/datafeed.js"></script>
                {this.props.styleTags}
            </Head>
            <body>
            <Main />
            <script async src="/static/js/sprite.js"></script>
            <NextScript />
            </body>
            </html>
        )
    }
}