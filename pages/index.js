import '../src/scss/style.scss'
import React from 'react'
import Router from 'next/router'
import Footer from '../layouts/Footer'
import JoinSection from '../layouts/JoinSection'
import BenefitsSection from '../layouts/BenefitsSection'
import HowSection from '../layouts/HowSection'
import RightWaySection from '../layouts/RightWay'
import TopSection from '../layouts/TopSection'
import Header from '../layouts/Header'
import isElemVisible from '../helper/helper';

export default class extends React.Component{
    reveal() {
        Array.from(document.querySelectorAll('.reveal')).forEach((el)=>{
            if(isElemVisible(el,0.8)){
                el.classList.add('in-viewport');
            }
        });
    }

    componentDidMount(){
        this.reveal();
        window.onscroll = () => {
            this.reveal();
            let y = window.pageYOffset || document.documentElement.scrollTop;
            Array.from(document.querySelectorAll('.js-draw')).forEach((el)=>{
                if(isElemVisible(el,1)){
                    el.classList.add('in-viewport');
                }
            });
            if(y > 0){
                document.querySelector('.header').classList.add('header--fixed');
            }
            else{
                document.querySelector('.header').classList.remove('header--fixed');
            }

        }
    }
    render(){
        return(
            <div>
                <Header />
                <TopSection />
                <RightWaySection />
                <HowSection />
                <BenefitsSection />
                <JoinSection />
                <Footer />
            </div>
        );
    }
}