import {Component} from 'react'

export default class SvgIcon extends Component{
    render(){
        return(
            <svg className={this.props.className} >
                <use xlinkHref={`#${this.props.icon}-svg`} />
            </svg>
        )
    }
}