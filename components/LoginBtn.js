import React, { Component } from 'react'

export default class LoginBtn extends Component{

    render(){
        return(
            <button className="login-btn">
                <i className="login-btn__icon"></i> Login
            </button>
        )
    }
}