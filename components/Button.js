import { Component } from 'react'
import classnames from 'classnames'


export default class Button extends Component{
    render(){
        const { className, ...props } = this.props;
        const cn = classnames(className, {'btn':true});
        return(
            <button className={cn} {...props}>
                {this.props.children}
            </button>
        )
    }
}