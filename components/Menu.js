import { Component } from 'react'
import ScrollToElement from '../helper/scrollto'

export default class Menu extends Component{
    constructor(props){
        super(props);
        this.scroller = new ScrollToElement();
    }

    scrollTo(e){
        e.preventDefault();
        let target = e.target.getAttribute('href');
        let el = document.querySelector(target);
        let header = document.querySelector('.header');
        header.classList.add('header--fixed');
        this.scroller.scrollToY(el.offsetTop - header.offsetHeight);
    }
    render(){
        return(
            <nav className="header__menu">
                <ul>
                    <li className="header__menu-item">
                        <a href="#how-it-works" onClick={this.scrollTo.bind(this)}>How it works</a>
                    </li>
                    <li className="header__menu-item">
                        <a href="#benefits" onClick={this.scrollTo.bind(this)}>Benefits</a>
                    </li>
                    <li className="header__menu-item">
                        <a href="#faq" onClick={this.scrollTo.bind(this)}>FAQ</a>
                    </li>
                </ul>
            </nav>
        )
    }
}