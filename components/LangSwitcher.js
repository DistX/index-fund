import React, { Component } from 'react'

export default class LangSwitcher extends Component{

    render(){
        return(
            <div className="lang-switcher">
                <div className="lang current">
                    <div className="lang__flag-container">
                        <img src="/static/images/en-flag.png" alt="flag"/>
                    </div>
                    <span className="lang-id">EN</span>
                </div>
                <div className="lang-other">
                    <div className="lang__flag-container">
                        <img src="/static/images/ru-flag.png" alt="flag"/>
                    </div>
                    <span className="lang-id">RU</span>
                </div>
            </div>
        )
    }
}