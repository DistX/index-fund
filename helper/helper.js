export default function isElemVisible(el, vF = 0.5) {

    let offset = _getOffset(el);

    let elemHeight = offset.height;
    let elemWidth = offset.width;
    let elemTop = offset.top;
    let elemLeft = offset.left;
    let elemBottom = elemTop + elemHeight;
    let elemRight = elemLeft + elemWidth;

    let top = elemTop + elemHeight * vF;
    let bottom = elemBottom - elemHeight * vF;

    let scrolled = _getScrolled();
    let viewTop = scrolled.y;
    let viewBottom = scrolled.y - 0 + window.document.documentElement.clientHeight;

    return top < viewBottom &&
        bottom > viewTop;
}

function _getOffset (domEl) {
    let offsetTop = 0
    let offsetLeft = 0

    // Grab the element’s dimensions.
    let offsetHeight = domEl.offsetHeight
    let offsetWidth = domEl.offsetWidth

    // Now calculate the distance between the element and its parent, then
    // again for the parent to its parent, and again etc... until we have the
    // total distance of the element to the document’s top and left origin.
    do {
        if (!isNaN(domEl.offsetTop)) {
            offsetTop += domEl.offsetTop
        }
        if (!isNaN(domEl.offsetLeft)) {
            offsetLeft += domEl.offsetLeft
        }
        domEl = domEl.offsetParent
    } while (domEl)

    return {
        top: offsetTop,
        left: offsetLeft,
        height: offsetHeight,
        width: offsetWidth
    }
}

function _getScrolled () {
    return {
        x: window.pageXOffset || document.documentElement.scrollLeft,
        y: window.pageYOffset || document.documentElement.scrollTop
    }
}