'use strict';

export default class ScrollToElement
{
  constructor(){
    this.target = {};
    this.currentPosition = 0;
    this.targetPosition = 0;
    this.duration = 600;
    this.currentTime = 0;
    this.incr = 20;
  };

  resetPositions(el){
    this.targetPosition = el.offsetTop;
    this.currentPosition = this.getCurrentPosition();
    this.currentTime = 0;
  }
  scrollToById(id){
    let el = document.querySelector(id);
    this.resetPositions(el);
    this.animateScroll();
  };
  scrollToY(y){
      this.targetPosition = y;
      this.currentPosition = this.getCurrentPosition();
      this.currentTime = 0;
      this.animateScroll();
  }
  animateScroll(){
    const self = this;
    let curTime = self.getTime();
    let val = this.easeInOutQuad(this.currentTime, this.currentPosition, this.targetPosition - this.currentPosition, this.duration);
    this.scroll(val);
    if(this.currentTime < this.duration){
      requestAnimationFrame( function(){self.animateScroll()} );
    }
  };
  easeInOutQuad(t,b,c,d){
    t /= d/2;
    if(t<1){
      return c/2*t*t + b;
    }
    t--;
    return -c/2 * (t*(t-2) -1) + b;
  };
  getTime(){
    this.currentTime += this.incr;
    return this.currentTime;
  }
  scroll(amount){
    document.documentElement.scrollTop = amount;
    document.body.parentNode.scrollTop = amount;
    document.body.scrollTop = amount;
  };
  getCurrentPosition(){
    return document.documentElement.scrollTop || document.body.parentNode.scrollTop || document.body.scrollTop;
  }
}
